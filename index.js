// Writing comments in Javascript: 
// There are two ways of writing comments in JS:
//  - single line comments - ctrl + /
// sample comments - can only make comments in a single line
/*

	multi line comments:
	ctrl + /
	
	Comments in JS, much like CSS and HTML, is not read by the browser.
	So, these comments are often used to add notes and to add markers to your code.

*/


console.log("Hello World");
/*

	Javascript - we can see log message in our console.

	Consoles are part of our browsers which will allow us to see/log messages, data or information from our programming language--Javascript.

	For most borowsers, console can be accessed through its developer tools in console tab.

	In fact, consoles in browsers allw us to add some Javascript expressions
	
	Statemenmts are instructions, expressions we add to our programming language which will then be communicated to our computers.

	Statements in javascript commonly ends in semicolon (;). However, javascript has an implemented way of automatically adding semicolons at the end of our statements. Which, therefore, mean that, unlike other langauges, JS does NOT require semicolons.

	Semicolons in JS are mostly used to mark the end of the statement.
	
	Syntax - is a set of rules that describes how statements are properly made/constructed.

	Lines/blocks of code must follow a certain set of rules for it to work. Because remember, you are not communicating with another human, in fact you are communication on your computer.

*/

console.log("Prince Joemarie Barro")

// Variables

/*

	In HTML, elements are containers of other elements and text. In javascript, variables are containers of data. agive a name to describe a piece of data.

	Variables also allow us to use or refer to data multiple times.
*/

let num = 10;

console.log(6);
console.log(num);

let name1 = "Jeffrey"

console.log("John")
console.log(name1)

/*Creating Variables*/

/*

	To create a variable, there are 2 steps to be done:
	-Declaration which actually allows to create the variable.
	-Initialization which allows to add an intial value to a variable. 

	Variables in JS are declared with the use of let or const keyword.

*/

let myVariable;
/*We can create variables without an initial value. However, when logged into the console, the variable will return a value of undefined.

	Undefined is a data type that indicates that variable does exist however there was no initial value.

*/
console.log(myVariable);

/*
	You can always initialize a variable after declaration by assigning a value to the variable with the use of the assignment operator (=)
*/

myVariable = "New Initialized Value"

console.log(myVariable);

/*
	You cannot and should not access a variable before its been created/declared.
*/
/*myVariable = "Initial Value";
let myVariable2;
console.log(myVariable2);
*/

/*
	Can you use or refer to a variable that has not been declared or created?
	No. This will result in an error: Not Defined.
	Undefined vs Not Defined
	Undefined means a variable has been declared but there is no initial value. Not defined means that the variable you are trying to refer or access does NOT exist.
		-Not defined is an error.

	Some errors in JS, will stop the program from further executing.
*/


let myVariable3 = "Another Sample";
console.log(myVariable3);

/*
	Variables must be declared first before they are used, referred to or accessed.
	Using, referring to, or accessing a variable before it's been declared results to an error.
*/

// Let vs Const

/*
	With the use of let, we can create variables that can be declared, initialized and re-assigned.

	In fact, we can declare let variables and initialize after.
*/

let bestFinalFantasy;
bestFinalFantasy = "Final Fantasy 6";
console.log(bestFinalFantasy);
/*Re-assigning let variables*/

bestFinalFantasy = "Final Fantasy 7";
console.log(bestFinalFantasy);
// Did the value change? Yes. We can re-assign values to let variables.
/*
	What happens when declaring another variable with the same name?
	It returns an error.

	Can you and should you create variables with the same name? No.
*/

/*let bestFinalFantasy = "Final Fantasy 10"
console.log(bestFinalFantasy)*/

// Const - const variables are variables with constant data. Therefore we should not re-declare,re-assign or even declare a const variable without initialization.
/*
const pi;
pi=3.1416;
console.log(pi)*/

// Can you re-assign another value to a const variable? No. You cannot and should re-assign values to a const variable.

const goat = "Michael Jordan"
console.log(goat)
/*goat = "Lebron James"
console.log(goat)*/

/*
	const variables are used for data that we expect or do not want its values to change.

*/
// Guides on variables Names
	/*

		1. When naming variables, it is important to create variables that are descriptive and indicative of the data it contains. 

			let firstName = "Michael"; - good variable name
			let pokemon = 25000; - bad variable name

			2. When naming variables, it is better to start witha lowercase letter. We usually avoid creating variable names that starts with capital letters. Because there are several keywords in JS that start in capital letters.

				let FirstName = "Michael"; - bad variable name
				let firstName = "Michael"; - good variable name

			3. Do not add spaces to your variable names. Use camelCase for multiple words, dashes or underscores.
			
				let first name = "Mike";
				camelCase is when we have first word in small caps and the next word added without space but is capitilaized:

					lastName emailAddress mobileNumber

				
				Underscores sample:

				let product_description = "lorem ipsum"
				let product_id = "250000ea1000"

	*/

	let num_sum = 50000;
	let numSum = 60000;
	console.log(num_sum);
	console.log(numSum);

	// Declaring Multiple Variables

		let brand = "Toyota", model = "Vios", type = "Sedan";
		console.log(brand);
		console.log(model);
		console.log(type);
		// console logging multiple variables: use commas to separate each variable.
		console.log(brand,model,type);

	// Data types
	/*
		In most programming languages, data is differentiated by their types. For most programming languages, you have to declare not only variable name but also the type of data you are saving into a variable, however, JS does not require this.

		To create data with particular data types, some data types require adding with a literal.
			string literals = '', "" and most recently: '' (template literals)
			object literals = {}
			array literals = []
	
	*/

	// String
	// Strings are a series of alphanumeric characters that a word, a phrase, a name or anything related to creating text.
	// String literals such as '' (single quotes) or ("") are used to write/create strings.
	let country ='Philppines';
	let province = "Metro Manila";

	console.log(country);
	console.log(province);

	let firstName = "Prince Joemarie", lastName = "Barro";
	console.log (firstName, lastName);

	// Concatenation
	/*Concatenation is a process/operation wherein we combine two strings as one.*/
	/* Javascript strings, spaces are also counted as characters.*/
	console.log(firstName + " " + lastName); 
	// firstName, lastName variables and the " ", were combined into a single string, however, the result was not saved. 
	let fullName = firstName+" "+lastName
	console.log(fullName)

	let word1 ="is";
	let word2 ="student";
	let word3 ="of";
	let word4 ="School";
	let word5 ="Zuitt Coding";
	let word6 ="a";
	let word7 ="Institute";
	let word8 ="Bootcamp";
	let space =".";

	let sentence = fullName + space + word1 + space + word2 + space + word3 + space + word4 + space + word5 + space + word6
	
	/*

		Template Literals ('') will allow us to create string with the use of backticks. Template Literals also allow us to easily concatenate strings without the use of + (plus). This is also allow to embed or add variables and even expressions in our string with the use of placeholders ${}.


	*/
	let = '${fullName} ${word1} ${word2} ${word3} ${word4} ${word5} ${word8}';

	console.log (sentence)

	// Number
		//Integers and floats. These are our number data which can be used for mathematical operations.

			let numString1 = "5" 
			let numString2 = "6"
			let num1 = 5
			let num2 = 6
			console.log(num1+num2)
			console.log(numString1 + numString2)
			let num3 = 5.5
			let num4 = .5
			console.log(num1+num3)
			console.log(num3+num4)


			// When the + or addition operator is used on numbers, it will do the proper mathematical operation. However, when used on strings, it will concatenate.

			// Forced coercion - when one data's type is forced to change to complete an operation.
			// String + num = concatenation

			console.log(numString1 + num1)
			console.log(num3 + numString2)

			// parseInt() - this can change the type of a numeric string to a proper number.
			console.log(num4+parseInt(numString1)) 

			let sum2 = num1 + parseInt(numString1)
			console.log(sum2)

	// Mathematical Operations (-,+,*,%)

		// Subtraction
		/*
			
		let numString1 = "5" 
			let numString2 = "6"
			let num1 = 5
			let num2 = 6
			let num3 = 5.5
			let num4 = .5

		*/
		console.log(num1-num3) 
		console.log(num3-num4)
		console.log(numString1-num2)	
		console.log(numString2-num2)

		let sample2 = "Jaybee"
		console.log(sample2 - numString1)

		// Multiplication
		console.log(num1*num2)
		console.log(numString1*num1)
		console.log(numString1*numString2)

		let product = num1*num2
		let product2 = numString1*num1
		let product3 = numString1*numString2

		// Division
		console.log (product/num2)
		console.log (product2/5)
		console.log (numString2/numString1)

		// Division/Multiplication by 0
		console.log(product2*0)
		console.log(product3/0)
		// Division by 0 is not accurately and should not be done it results to infinity


		// Modulo - reminder of a division operation
		console.log(product2%num2)
		console.log(product3%product2)
		console.log(num1%num2)
		console.log(num1%num1)
		console.log(product2%num1)
		console.log(product3%num2)

	// Boolean
		
		let isAdmin = true;
		let isMarried = false;
		let isMVP = true;

		// You can also concatenate strings + boolean
		console.log("Is she married?" + isMarried); 
		console.log("Is he the MVP?" + isMVP);
		console.log('Is he the current admin?' +isAdmin);

	// Arrays - are a special kind of data type used to store multiple values.
	// Arrays can actually store data with different types BUT as the best practice arrays are used to contain multiple values of the SAME data type.
	// Values in a array are separated by commas
	// An array is created with an Array Literal = []

		let array1= ["Goku", "Piccolo", "Gohan", "Vegeta"]
		console.log(array1)
		let array2 = ["One Punch Man", true,500, "Saitama"]
		console.log(array2)

		// In the future sessions, we will be introduced to array methods which will allow us to manipulate our arrays. Having an array with differring data types may obstruct from the proper use of these methods. Furthermore, if want to group differing data types we can use Objects instead.

		// Arrays are better thought of as groups of data.

	// Objects
		// Objects are another special kind of data type used to mimic real world objects
		// Used to create complex data that contain pieces of information that are relevant to each other.
		// Object are created with object literals = {}
		// Each data are paired with a key.
		// Each fied is called a property.
		// Each field is separated by a comma. 

		let heo1 = {

			heroName: "One Punch Man",
			isActive: true,
			salary: 500,
			realName: "Saitama"
		};

		console.log(heo1.heroName);

		let bandMember = ["Danny", "Boboy", "Jim"]
		let person1 = {
			firstName: "Prince"
			isDeveloper: true,
			hasPortfolio: true,
			age: 25,
		}

		let myQuarterGrades = {
			firstQuarter: 93,
			secondQuarter: 89,
			thirdQuarter: 95,
			fourthQuarter: 98,
		}

		let grades = [93,89,95,98]

	// Undefined vs Null

		// Null - is explicit absence data. This is done to project that a variable contains nothing over undefined merely means there is no data in the variable BECAUSE the variable has not been assigned an initial value.

		let sampleNull = null;

		// Undefined - is a representation that a variable has been declared but it was not assigned  an initial value:

		let sampleUndefined;

		console.log(sampleNull);
		console.log(sampleUndefined);

		// Certain processes in programming explicity return null to indicate that the task resulted to nothing.

		let foundResult = null;

		// For undefined, this is normally caused by developers creating variables that have no value or data/associated with them.
		// This is when a variable does exist but its value is still unknown.
		let person2 = {
			name: "Peter",
			age: 35
		}
		// because person2 does exist but the property isAdmin does not.
		console.log(person2.isAdmin)
	//Functions
	//  a parameter acts a named variable
	// You cannot get the value of a parameter outside of its function.
	/*console.log(name);*/
	// When a function is invoked and data is passed, we call the data as argument.
	// In this invocation, "Jeff" is an agrument passed into our printName function and this represented by the







	function display (msg) {

			console.log (msg);
	}

	displayMsg("Javascript is fun");

	// Multiple Parameters and arguments
	// A function can not only recieve a single argument but it can also recieve multiple arguments as long as it matches the number of parameters in the function.

		function displayFullName(firstName,lastName){
			console.log('${firstName} ${lastName} is ${25}');
		};

		displayFullName("Prince", "Barro", 29);

 	//Return keyword

 	 // return keyword so that a function may return a value.
 	 // it aslo stops the prcoess of the function that any other instructions after the keyword will not be processed

 	 function createFullName(firstName,middleName,lastName) {
 	 	return '${firstName} ${middleName} ${lastName}'
 	 }
 	 // result of this function can be saved into a variable
 	 // The result of a function without a return keyword will not save the result in a variable.

 	 let fullName1 = createFullName("Stephen","Wardell","Curry");
 	 let fullName2 = displayFullName ("Tom", "Cruise", "Mapother");

 	 console.log(fullName1);
 	 console.log(fullName2);